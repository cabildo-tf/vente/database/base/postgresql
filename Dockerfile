FROM mdillon/postgis:11

LABEL maintainer="info@gesplan.es"

RUN apt-get update \
      && apt-cache showpkg "postgresql-$PG_MAJOR-pgrouting" \
      && apt-get install -y --no-install-recommends \
           "postgresql-$PG_MAJOR-pgrouting=3.*" \
           "postgresql-$PG_MAJOR-pgrouting-scripts=3.*" \
      && rm -rf /var/lib/apt/lists/* && \
	sed -i -e 's/# es_ES.UTF-8 UTF-8/es_ES.UTF-8 UTF-8/' /etc/locale.gen && \
	sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen && \
    mv "/docker-entrypoint-initdb.d/postgis.sh" "/docker-entrypoint-initdb.d/01_postgis.sh"


ENV	LANG="en_US.UTF8" \
	LC_COLLATE="es_ES.UTF-8" \
	LC_CTYPE="es_ES.UTF-8" \
	LC_MONETARY="es_ES.UTF-8" \
	LC_NUMERIC="es_ES.UTF-8" \
	LC_TIME="es_ES.UTF-8" \
	LD_LIBRARY_PATH="/usr/local/lib/postgresql"

COPY rootfs /
