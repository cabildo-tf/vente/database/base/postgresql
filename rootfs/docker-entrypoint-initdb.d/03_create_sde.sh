#!/bin/bash
set -e

function init_sde() {

	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
		-- Create SDE user and schema
		CREATE USER sde SUPERUSER INHERIT;
		ALTER USER sde WITH PASSWORD '${POSTGRES_SDE_PASSWORD}';
		
		CREATE SCHEMA sde AUTHORIZATION sde;
		ALTER DATABASE ${POSTGRES_DB} SET SEARCH_PATH="\$user",sde,public;

	EOSQL
}

if [ ! -z "${POSTGRES_SDE_PASSWORD}" ] 
then
	echo "Init config for sde"
	init_sde
fi